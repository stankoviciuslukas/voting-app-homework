## Requirements

- Docker (20.10.21 or above)
- kind (v1.27 or above)
- Helm (v3.12 or above)

## Create kind cluster

Add additional IP to bind kind ports. (Sometimes through localhost ingress doesn't work correctly, so we just add an additional IP to bind to.)

```
sudo ip a a 10.0.0.10 dev <default_interface>
```

With ingress configuration

```
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
    listenAddress: "10.0.0.10"
EOF
```

## Deploy nginx ingress controller

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```

## Deploy example-voting-app release

```
helm dependency update
helm upgrade --install example-voting-app .
```

NOTE: Not using bitnami chart for deploying redis because application has an hardcoded redis host that cannot be changed on bitnami chart. Therefore using other provider chart for redis release.

## Add ingress to host file

```
sudo echo "10.0.0.10 result.voting.com" >> /etc/hosts
sudo echo "10.0.0.10 vote.voting.com" >> /etc/hosts
```

## Validate hosts

```
curl result.voting.com -I
curl vote.voting.com -I
```

{{/*
Expand the name of the chart.
*/}}
{{- define "example-voting-app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "example-voting-app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "example-voting-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}

{{- define "example-voting-app.ingress.labels" -}}
helm.sh/chart: {{ include "example-voting-app.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "example-voting-app.vote.labels" -}}
helm.sh/chart: {{ include "example-voting-app.chart" . }}
{{ include "example-voting-app.vote.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "example-voting-app.result.labels" -}}
helm.sh/chart: {{ include "example-voting-app.chart" . }}
{{ include "example-voting-app.result.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "example-voting-app.worker.labels" -}}
helm.sh/chart: {{ include "example-voting-app.chart" . }}
{{ include "example-voting-app.worker.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}

{{- define "example-voting-app.vote.selectorLabels" -}}
app.kubernetes.io/name: {{ include "example-voting-app.name" . }}-vote
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "example-voting-app.result.selectorLabels" -}}
app.kubernetes.io/name: {{ include "example-voting-app.name" . }}-result
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "example-voting-app.worker.selectorLabels" -}}
app.kubernetes.io/name: {{ include "example-voting-app.name" . }}-worker
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "example-voting-app.vote.fullname" -}}
{{- printf "%s-%s" .Release.Name "vote" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "example-voting-app.result.fullname" -}}
{{- printf "%s-%s" .Release.Name "result" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "example-voting-app.worker.fullname" -}}
{{- printf "%s-%s" .Release.Name "worker" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

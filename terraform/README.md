Terraform resources created/plan:

- EKS
- VPC
- Security Group allowing 80 and 443 ports for incoming traffic and allowing all outgoing traffic
- Network Load Balancer routing 80 and 443 traffic

Regarding production context there should be:
    - Certificates for HTTPs traffic
    - High Availability across multiple Regions/AZ
    - Security measures such as non root user application, restrict syscalls and so on
    - Autoscaling could be achieved using AutoScaling Groups and measuring CPU usages from CloudWatch metrics. Depending on the specified thresholds autoscaling could scale nodes and in turn pods that receive incoming traffic.
    - As for deployment there could be an HPA (Horitizontal Pod Autoscaler) that could scale pods depending on various metrics (not only CPU) using prometheus adapter or keda

Diagram:

![](diagram.png)

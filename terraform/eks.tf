module "example_voting_app_eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.16.0"

  cluster_name    = "example-voting-app-eks"
  cluster_version = "1.27"

  vpc_id                         = module.example_voting_app_vpc.vpc_id
  subnet_ids                     = module.example_voting_app_vpc.private_subnets
  cluster_endpoint_public_access = true

  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"
  }

  eks_managed_node_groups = {
    one = {
      name = "node-group-1"

      instance_types = ["t3.small"]

      min_size     = 1
      max_size     = 3
      desired_size = 2
    }

  }

  cluster_security_group_additional_rules = {
    ingress_http_access = {
      description                   = "http access"
      from_port                     = 0
      to_port                       = 80
      protocol                      = "TCP"
      type                          = "ingress"
      source_cluster_security_group = true
      cidr_blocks                   = ["0.0.0.0/0"]
      ipv6_cidr_blocks              = ["::/0"]
    }

    ingress_https_access = {
      description                   = "https access"
      from_port                     = 0
      to_port                       = 443
      protocol                      = "TCP"
      type                          = "ingress"
      source_cluster_security_group = true
      cidr_blocks                   = ["0.0.0.0/0"]
      ipv6_cidr_blocks              = ["::/0"]
    }

    egress_access = {
      from_port                     = 0
      to_port                       = 0
      protocol                      = "-1"
      cidr_blocks                   = ["0.0.0.0/0"]
      ipv6_cidr_blocks              = ["::/0"]
      type                          = "egress"
      source_cluster_security_group = true
    }
  }

  // Security Group
  // LoadBalancer
  // AutoScale
  // Grafikas
  // How to do differently
}

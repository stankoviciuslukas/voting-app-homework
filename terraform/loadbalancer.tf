module "nlb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "8.7.0"

  name = "example-voting-app-alb"

  load_balancer_type = "network"

  vpc_id  = module.example_voting_app_vpc.vpc_id
  subnets = module.example_voting_app_vpc.private_subnets

  target_groups = [
    {
      name_prefix      = "node-"
      backend_protocol = "TCP"
      backend_port     = 80
      target_type      = "ip"
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "TLS"
      certificate_arn    = "arn:aws:iam::123456789012:server-certificate/test_cert-123456789012"
      target_group_index = 0
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  tags = {
    Environment = "demo"
  }
}
